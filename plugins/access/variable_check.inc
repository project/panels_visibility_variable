<?php

/**
 * @file
 * Plugin to check a variable.
 */
 
$plugin = array(
  'title' => t('Variable'),
  'description' => t('Checks a variable.'),
  'callback' => 'panels_visibility_variable_ctools_access_check',
  'default' => array('negate' => 0),
  'summary' => 'panels_visibility_variable_ctools_access_summary',
  'settings form' => 'panels_visibility_variable_ctools_access_settings',
);

/**
 * Settings form for the 'variable' access plugin.
 */
function panels_visibility_variable_ctools_access_settings($form, &$form_state, $conf) {
  $form['settings']['variable'] = array(
    '#title' => t('Variable'),
    '#type' => 'textfield',
    '#description' => t('Type the name of the variable you want to check.'),
    '#default_value' => $conf['variable'],
    '#required' => TRUE,
  );
  return $form;
}

/**
 * Provide a summary description.
 */
function panels_visibility_variable_ctools_access_summary($conf, $context) {
  $replacement = array('@keyword' => $conf['variable']);
  return t('Checks if variable "@keyword" is enabled.', $replacement);
}

/**
 * Check for access.
 */
function panels_visibility_variable_ctools_access_check($conf, $context) {
  $var = variable_get($conf['variable']);
  if ($var) {
    return TRUE;
  }
  return FALSE;
}
